# Maintainer: Jeff Dickey <alpine@rtx.pub>
pkgname=rtx
pkgver=2023.11.0
pkgrel=0
pkgdesc="Polyglot runtime and dev tool version manager"
url="https://rtx.pub"
arch="all !s390x !riscv64 !ppc64le" # limited by cargo
license="MIT"
makedepends="cargo bash direnv cargo-auditable openssl-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/jdxcode/rtx/archive/refs/tags/v$pkgver.tar.gz"

prepare() {
	default_prepare
	git init .
	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --release --frozen --bin rtx
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/rtx \
		-t "$pkgdir"/usr/bin/
}

sha512sums="
29ef71bd654c027602f1db99d6157e8399d16fe087b1ea88db5b7cb05bf19cb051279b784efd5e69eac1613d7d1dbd3300f022314f8295edd350208ee095e8d7  rtx-2023.11.0.tar.gz
"
