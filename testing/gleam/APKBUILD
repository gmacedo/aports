# Contributor: rubicon <rubicon@mailo.com>
# Maintainer: Celeste <cielesti@protonmail.com>
pkgname=gleam
pkgver=0.32.2
pkgrel=0
pkgdesc="Statically-typed language that compiles to Erlang and JS"
url="https://gleam.run/"
# s390x, riscv64, ppc64le: ring
# armhf: error: Undefined temporary symbol .LBB88_2
arch="all !armhf !s390x !riscv64 !ppc64le"
license="Apache-2.0"
depends="erlang-dev"
makedepends="cargo cargo-auditable"
source="$pkgname-$pkgver.tar.gz::https://github.com/gleam-lang/gleam/archive/refs/tags/v$pkgver.tar.gz"

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release
}

check() {
	cargo test --frozen
}

package() {
	install -Dvm755 target/release/gleam -t "$pkgdir"/usr/bin/
}

sha512sums="
0152a3ba29f524b30963bc0d12e42b712d7ed5279285eac67abb17a2667b9af467e4a1a8d9e49950b5345bb8c544ecbc6ce1bab9d3c2c2b25471746cfa022e2e  gleam-0.32.2.tar.gz
"
